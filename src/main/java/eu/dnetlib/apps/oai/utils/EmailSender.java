package eu.dnetlib.apps.oai.utils;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import eu.dnetlib.apps.oai.model.CollectionInfo;
import eu.dnetlib.apps.oai.model.ExecutionStatus;
import jakarta.annotation.PostConstruct;

@Service
public class EmailSender {

	private static final Log log = LogFactory.getLog(EmailSender.class);

	private final BlockingQueue<Message> queue = new LinkedBlockingQueue<>();

	@Value("${oai.conf.app.public_url}")
	private String publicUrl;

	@Value("${oai.conf.notification.sender.name}")
	private String fromName;

	@Value("${oai.conf.notification.sender.email}")
	private String fromEmail;

	@Value("${oai.conf.notification.smtp.host}")
	private String smtpHost;

	@Value("${oai.conf.notification.smtp.port}")
	private long smtpPort;

	@Value("${oai.conf.notification.smtp.user}")
	private String smtpUser;

	@Value("${oai.conf.notification.smtp.password}")
	private String smtpPassword;

	@Autowired
	private TemplateEngine templateEngine;

	@PostConstruct
	public void init() {

		new Thread(() -> {
			while (true) {
				try {
					final Message message = this.queue.take();
					if (message != null) {
						try {
							log.info("Sending mail...");

							log.debug(message.getContent());

							Transport.send(message);

							log.info("...sent");
						} catch (final Throwable e) {
							log.error("Error sending email", e);
						}
					}
				} catch (final InterruptedException e1) {
					throw new RuntimeException(e1);
				}
			}
		}).start();
	}

	public void sendNotification(final CollectionInfo info) {
		if (StringUtils.isNotBlank(info.getNotificationEmail())) {
			final String to = info.getNotificationEmail();
			if (info.getExecutionStatus() == ExecutionStatus.COMPLETED) {
				final String subject = String.format("Success Notification: Metadata Collection Request (ID: %s)", info.getId());
				sendMail(to, subject, prepareMessage("email/success", info));
			} else {
				final String subject = String.format("Failure Notification: Metadata Collection Request (ID: %s)", info.getId());
				sendMail(to, subject, prepareMessage("email/failure", info));
			}
		}
	}

	private String prepareMessage(final String template, final CollectionInfo info) {
		final Context context = new Context();
		context.setVariable("info", info);
		context.setVariable("baseUrl", publicUrl);
		return templateEngine.process(template, context);
	}

	public void sendMail(final String to, final String subject, final String message) {
		try {
			final Session session = Session.getInstance(obtainProperties(), obtainAuthenticator());

			final MimeMessage mimeMessage = new MimeMessage(session);
			mimeMessage.setFrom(new InternetAddress(fromEmail, fromName));
			mimeMessage.setSubject(subject);
			mimeMessage.setContent(message, "text/html; charset=utf-8");
			mimeMessage.setSentDate(new Date());

			mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			this.queue.add(mimeMessage);

			log.info("Mail to " + to + " in queue (size=" + queue.size() + ")");
		} catch (final Exception e) {
			log.error("Error sending mail", e);
		}
	}

	private Properties obtainProperties() {
		final Properties p = new Properties();
		p.put("mail.transport.protocol", "smtp");
		p.put("mail.smtp.host", smtpHost);
		p.put("mail.smtp.port", smtpPort);
		p.put("mail.smtp.auth", Boolean.toString(StringUtils.isNotBlank(smtpUser)));
		return p;
	}

	private Authenticator obtainAuthenticator() {
		if (StringUtils.isBlank(smtpUser)) { return null; }

		return new Authenticator() {

			private final PasswordAuthentication authentication = new PasswordAuthentication(smtpUser, smtpPassword);

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return authentication;
			}

		};
	}

}
