package eu.dnetlib.apps.oai.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

public class SimpleUtils {

	private static final String UTF_8 = StandardCharsets.UTF_8.toString();

	private static final DateTimeFormatter oaiDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public static String generateNewJobId() {
		return "coll-" + UUID.randomUUID();
	}

	public static String oaiFirstUrl(final String baseUrl, final String format, final String setSpec, final LocalDateTime from, final LocalDateTime until) {
		try {
			String url = baseUrl + "?verb=ListRecords&metadataPrefix=" + URLEncoder.encode(format, UTF_8);

			if (setSpec != null && !setSpec.isEmpty()) {
				url += "&set=" + URLEncoder.encode(setSpec, UTF_8);
			}
			if (from != null) {
				url += "&from=" + URLEncoder.encode(from.format(oaiDateFormatter), UTF_8);
			}
			if (until != null) {
				url += "&until=" + URLEncoder.encode(until.format(oaiDateFormatter), UTF_8);
			}
			return url;
		} catch (final UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String oaiNextUrl(final String baseUrl, final String rtoken) {
		try {
			if (StringUtils.isNotBlank(rtoken)) {
				return baseUrl + "?verb=ListRecords&resumptionToken=" + URLEncoder.encode(rtoken, UTF_8);
			} else {
				return null;
			}
		} catch (final UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static String oaiIdToFilename(final String id) {
		return DigestUtils.md5Hex(id) + ".xml";
	}

	public static String pageToDir(final long page) {
		return "page_" + StringUtils.leftPad(Long.toString(page), 4, "0");
	}
}
