package eu.dnetlib.apps.oai.utils;

import java.io.IOException;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import eu.dnetlib.apps.oai.model.CollectionCall;
import eu.dnetlib.apps.oai.model.ExecutionStatus;

public class HttpFetcher {

	public static String download(final CollectionCall call) throws IOException {

		try (final CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
			call.setStatus(ExecutionStatus.RUNNING);
			return httpClient.execute(new HttpGet(call.getUrl()), response -> {
				final int code = response.getCode();
				call.setResponseCode(response.getCode());

				if (code >= 200 && code < 300 && response.getEntity() != null) {
					call.setStatus(ExecutionStatus.COMPLETED);
					return EntityUtils.toString(response.getEntity());
				} else {
					call.setStatus(ExecutionStatus.FAILED);
					throw new IOException("Invalid http response");
				}
			});
		} catch (final Throwable e) {
			call.setStatus(ExecutionStatus.FAILED);
			throw new IOException(e);
		}
	}
}
