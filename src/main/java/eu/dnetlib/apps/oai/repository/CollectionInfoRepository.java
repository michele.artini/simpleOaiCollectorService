package eu.dnetlib.apps.oai.repository;

import java.time.LocalDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import eu.dnetlib.apps.oai.model.CollectionInfo;

public interface CollectionInfoRepository extends JpaRepository<CollectionInfo, String> {

	@Modifying
	@Query("update CollectionInfo c set c.expirationDate = ?1 where c.expirationDate > ?1")
	void forceExpirationDate(LocalDateTime date);
}
