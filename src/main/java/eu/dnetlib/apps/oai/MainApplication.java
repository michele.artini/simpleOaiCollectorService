package eu.dnetlib.apps.oai;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;

@SpringBootApplication
@EnableScheduling
public class MainApplication {

	@Value("${oai.conf.app.public_url}")
	private String swaggerPublicUrl;

	@Value("${oai.conf.app.main_title}")
	private String swaggerMainTitle;

	@Value("${oai.conf.app.api_title}")
	private String swaggerApiTitle;

	@Value("${oai.conf.app.api_desc}")
	private String swaggerApiDesc;

	@Value("${oai.conf.app.api_version}")
	private String swaggerApiVersion;

	private static final License AGPL_3_LICENSE =
		new License().name("GNU Affero General Public License v3.0 or later").url("https://www.gnu.org/licenses/agpl-3.0.txt");

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}

	// Beans for Swagger

	@Bean
	public OpenAPI newSwaggerDocket() {
		final List<Server> servers = new ArrayList<>();
		if (StringUtils.isNotBlank(swaggerPublicUrl)) {
			final Server server = new Server();
			server.setUrl(swaggerPublicUrl);
			server.setDescription(swaggerMainTitle);
			servers.add(server);
		}
		return new OpenAPI()
			.servers(servers)
			.info(getSwaggerInfo())
			.tags(swaggerTags());
	}

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
			.group(swaggerApiTitle)
			.pathsToMatch("/api/**")
			.build();
	}

	private Info getSwaggerInfo() {
		return new Info()
			.title(swaggerApiTitle)
			.description(swaggerApiDesc)
			.version(swaggerApiVersion)
			.license(AGPL_3_LICENSE);
	}

	protected List<Tag> swaggerTags() {
		return new ArrayList<>();
	}

}
