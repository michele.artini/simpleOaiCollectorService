package eu.dnetlib.apps.oai.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;

import eu.dnetlib.apps.oai.utils.SimpleUtils;

public class FtpStorage extends StorageClient {

	private static final Log log = LogFactory.getLog(FtpStorage.class);

	private final FTPClient ftp;

	private long currentPage = -1;

	public FtpStorage(final String jobId,
		final URI storageBasePath,
		final String ftpUser,
		final String ftpPassword) {

		super(jobId, storageBasePath);

		final String protocol = storageBasePath.getScheme();
		final String host = storageBasePath.getHost();
		final int port = storageBasePath.getPort();

		this.ftp = ftpConnect(host, port, protocol.equalsIgnoreCase("ftps"));

		ftpLogin(ftpUser, ftpPassword);
		changeDir(storageBasePath.getPath());
		changeDir(jobId);
	}

	private FTPClient ftpConnect(final String server, final int port, final boolean secure) {
		try {
			if (secure) {
				final FTPSClient ftp = new FTPSClient();
				ftp.connect(server, port > 0 ? port : FTPSClient.DEFAULT_FTPS_PORT);
				// Set protection buffer size
				ftp.execPBSZ(0);
				// Set data channel protection to private
				ftp.execPROT("P");
				return ftp;
			} else {
				final FTPClient ftp = new FTPClient();
				ftp.connect(server, port > 0 ? port : FTPClient.DEFAULT_PORT);
				return ftp;
			}
		} catch (final IOException e) {
			log.error("Ftp Connection Failed", e);
			throw new RuntimeException(e);
		}
	}

	private void ftpLogin(final String user, final String password) {
		try {
			if (!ftp.login(user, password)) { throw new RuntimeException("FTP login failed"); }
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			ftp.enterLocalPassiveMode();
			ftp.setBufferSize(1024);
			log.info("Ftp logged");
		} catch (final IOException e) {
			log.error("Ftp Login Failed", e);
			complete();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void complete() {
		if (ftp != null && ftp.isConnected()) {
			try {
				ftp.disconnect();
				log.info("Ftp Disconnected");
			} catch (final IOException e) {
				log.error("Ftp Disconnection Failed");
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public void prepareCurrentPage(final long page) {
		if (page != this.currentPage) {
			this.currentPage = page;
			changeDir("..");
			changeDir(SimpleUtils.pageToDir(page));
		}
	}

	private boolean changeDir(final String dir) {
		try {
			if (!ftp.changeWorkingDirectory(dir)) {
				ftp.makeDirectory(dir);
				return ftp.changeWorkingDirectory(dir);
			}
			return true;
		} catch (final IOException e) {
			log.error("Error changing or create dir: " + dir);
			complete();
			throw new RuntimeException("Error changing or create dir: " + dir, e);
		}
	}

	@Override
	public void saveFile(final String filename, final String content) {
		try (InputStream is = new ByteArrayInputStream(content.getBytes())) {
			if (log.isDebugEnabled()) {
				log.debug("Saving file " + filename);
				log.debug(content);
			}
			if (!ftp.storeFile(filename, is)) {
				log.error("Error saving file: " + ftp.getReplyCode() + " - " + ftp.getReplyString());
				throw new RuntimeException("Error saving file: " + ftp.getReplyString());
			}
		} catch (final IOException e) {
			log.error("Error saving info file");
			throw new RuntimeException("Error saving info file", e);
		}
	}

}
