package eu.dnetlib.apps.oai.storage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.apps.oai.utils.SimpleUtils;

public class LocalStorage extends StorageClient {

	private static final Log log = LogFactory.getLog(LocalStorage.class);

	private long currentPage = -1;

	private String rootDir;

	public LocalStorage(final String jobId, final URI storageBasePath) {
		super(jobId, storageBasePath);

		try {
			final File d = new File(storageBasePath.getPath() + "/" + jobId);
			FileUtils.forceMkdir(d);
			this.rootDir = d.getAbsolutePath();
		} catch (final IOException e) {
			throw new RuntimeException("Errore creating root dir", e);
		}
	}

	@Override
	public void prepareCurrentPage(final long page) {
		try {
			final File d = new File(rootDir + "/" + SimpleUtils.pageToDir(page));
			FileUtils.forceMkdir(d);
			this.currentPage = page;
		} catch (final IOException e) {
			throw new RuntimeException("Errore creating page", e);
		}
	}

	@Override
	public void saveFile(final String filename, final String body) {
		try (FileWriter fw = new FileWriter(this.rootDir + "/" + SimpleUtils.pageToDir(this.currentPage) + "/" + filename)) {
			IOUtils.write(body, fw);
		} catch (final IOException e) {
			log.error("Error saving info file");
			throw new RuntimeException("Error saving info file", e);
		}
	}

}
