package eu.dnetlib.apps.oai.storage;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class StorageClientFactory {

	@Value("${oai.conf.storage.basePath}")
	private URI storageBasePath;

	@Value("${oai.conf.storage.user}")
	private String storageUser;

	@Value("${oai.conf.storage.password}")
	private String storagePassword;

	public StorageClient newClient(final String jobId) {

		final String protocol = storageBasePath.getScheme();

		if (protocol.equalsIgnoreCase("ftp") || protocol.equalsIgnoreCase("ftps")) {
			return new FtpStorage(jobId, storageBasePath, storageUser, storagePassword);
		} else if (protocol.equalsIgnoreCase("file")) {
			return new LocalStorage(jobId, storageBasePath);
		} else if (protocol.equalsIgnoreCase("zip")) {
			return new ZipStorage(jobId, storageBasePath);
		} else {
			throw new RuntimeException("Invalid storage protocol: " + protocol + " (valid protocol are: file, ftp and ftps)");
		}

	}

}
