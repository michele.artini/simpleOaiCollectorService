package eu.dnetlib.apps.oai.storage;

import java.net.URI;

public abstract class StorageClient {

	private final String jobId;
	private final URI storageBasePath;

	public StorageClient(final String jobId, final URI storageBasePath) {
		this.jobId = jobId;
		this.storageBasePath = storageBasePath;
	}

	public void complete() {};

	abstract public void prepareCurrentPage(long page);

	abstract public void saveFile(String filename, String body);

	public String getStorageUrl() {
		return storageBasePath + "/" + jobId;
	}

}
