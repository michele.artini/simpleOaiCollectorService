package eu.dnetlib.apps.oai.storage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.apps.oai.utils.SimpleUtils;

public class ZipStorage extends StorageClient {

	private static final Log log = LogFactory.getLog(ZipStorage.class);

	private FileOutputStream fos;
	private ZipOutputStream zipOut;
	private long currPage = -1;

	public ZipStorage(final String jobId, final URI storageBasePath) {
		super(jobId, storageBasePath);

		try {
			final File rootDir = new File(storageBasePath.getPath());
			FileUtils.forceMkdir(rootDir);

			this.fos = new FileOutputStream(rootDir.getAbsolutePath() + "/" + jobId + ".zip");
			this.zipOut = new ZipOutputStream(fos);
		} catch (final IOException e) {
			throw new RuntimeException("Error preparing zip", e);
		}
	}

	@Override
	public void complete() {
		try {
			if (zipOut != null) {
				zipOut.close();
			}
			if (fos != null) {
				fos.close();
			}
		} catch (final IOException e) {
			log.error("Ftp Disconnection Failed");
			throw new RuntimeException(e);
		}
	}

	@Override
	public void prepareCurrentPage(final long page) {
		try {
			zipOut.putNextEntry(new ZipEntry(SimpleUtils.pageToDir(page) + "/"));
			zipOut.closeEntry();
			this.currPage = page;
		} catch (final IOException e) {
			throw new RuntimeException("Error adding a directory to zip", e);
		}

	}

	@Override
	public void saveFile(final String filename, final String body) {
		try {
			zipOut.putNextEntry(new ZipEntry(new ZipEntry(SimpleUtils.pageToDir(currPage) + "/" + filename)));
			zipOut.write(body.getBytes());
		} catch (final IOException e) {
			throw new RuntimeException("Error adding a file to zip", e);
		}
	}

	@Override
	public String getStorageUrl() {
		return super.getStorageUrl() + ".zip";
	}

}
