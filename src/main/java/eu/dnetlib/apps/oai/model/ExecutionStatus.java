package eu.dnetlib.apps.oai.model;

public enum ExecutionStatus {
	READY,
	RUNNING,
	COMPLETED,
	FAILED,
	EXPIRED
}
