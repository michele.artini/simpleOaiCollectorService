package eu.dnetlib.apps.oai.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "collection_history")
public class CollectionInfo implements Serializable {

	private static final long serialVersionUID = 5843197167336338295L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "oai_base_url", length = 1024)
	private String oaiBaseUrl;

	@Column(name = "oai_format")
	private String oaiFormat;

	@Column(name = "oai_set")
	private String oaiSet;

	@Column(name = "oai_from")
	private LocalDateTime oaiFrom;

	@Column(name = "oai_until")
	private LocalDateTime oaiUntil;

	@Column(name = "storage_url", length = 1024)
	private String storageUrl;

	@Column(name = "public_url", length = 1024)
	private String publicUrl;

	@Column(name = "start_date")
	private LocalDateTime start;

	@Column(name = "end_date")
	private LocalDateTime end;

	@Column(name = "expiration_date")
	private LocalDateTime expirationDate;

	@Column(name = "max")
	private long max = Long.MAX_VALUE;

	@Column(name = "status")
	private ExecutionStatus executionStatus;

	@Column(name = "total")
	private long total = 0;

	@Column(name = "notification_email")
	private String notificationEmail;

	@Lob
	@Column(name = "message")
	private String message = "";

	@Transient
	private final List<CollectionCall> calls = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getOaiBaseUrl() {
		return oaiBaseUrl;
	}

	public void setOaiBaseUrl(final String oaiBaseUrl) {
		this.oaiBaseUrl = oaiBaseUrl;
	}

	public String getOaiFormat() {
		return oaiFormat;
	}

	public void setOaiFormat(final String oaiFormat) {
		this.oaiFormat = oaiFormat;
	}

	public String getOaiSet() {
		return oaiSet;
	}

	public void setOaiSet(final String oaiSet) {
		this.oaiSet = oaiSet;
	}

	public LocalDateTime getOaiFrom() {
		return oaiFrom;
	}

	public void setOaiFrom(final LocalDateTime oaiFrom) {
		this.oaiFrom = oaiFrom;
	}

	public LocalDateTime getOaiUntil() {
		return oaiUntil;
	}

	public void setOaiUntil(final LocalDateTime oaiUntil) {
		this.oaiUntil = oaiUntil;
	}

	public String getStorageUrl() {
		return storageUrl;
	}

	public void setStorageUrl(final String storageUrl) {
		this.storageUrl = storageUrl;
	}

	public String getPublicUrl() {
		return publicUrl;
	}

	public void setPublicUrl(final String publicUrl) {
		this.publicUrl = publicUrl;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(final LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(final LocalDateTime end) {
		this.end = end;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(final LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public long getMax() {
		return max;
	}

	public void setMax(final long max) {
		this.max = max;
	}

	public ExecutionStatus getExecutionStatus() {
		return executionStatus;
	}

	public void setExecutionStatus(final ExecutionStatus executionStatus) {
		this.executionStatus = executionStatus;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(final long total) {
		this.total = total;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(final String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public List<CollectionCall> getCalls() {
		return calls;
	}

}
