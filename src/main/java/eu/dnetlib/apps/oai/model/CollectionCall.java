package eu.dnetlib.apps.oai.model;

import java.io.Serializable;
import java.util.Objects;

public class CollectionCall implements Serializable {

	private static final long serialVersionUID = 4915954425467830605L;

	private String url;
	private ExecutionStatus status = ExecutionStatus.READY;
	private int responseCode;
	private long numberOfRecords = 0;

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public ExecutionStatus getStatus() {
		return status;
	}

	public void setStatus(final ExecutionStatus status) {
		this.status = status;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(final int responseCode) {
		this.responseCode = responseCode;
	}

	public long getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(final long numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	@Override
	public int hashCode() {
		return Objects.hash(url);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		final CollectionCall other = (CollectionCall) obj;
		return Objects.equals(url, other.url);
	}

}
