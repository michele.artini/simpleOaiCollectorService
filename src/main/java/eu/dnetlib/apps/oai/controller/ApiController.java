package eu.dnetlib.apps.oai.controller;

import java.time.LocalDateTime;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import eu.dnetlib.apps.oai.model.CollectionInfo;
import eu.dnetlib.apps.oai.model.ExecutionStatus;
import eu.dnetlib.apps.oai.service.CollectorService;

@RestController
@RequestMapping("/api")
public class ApiController {

	private static final Log log = LogFactory.getLog(ApiController.class);

	@Autowired
	private CollectorService service;

	@GetMapping("/collect")
	public CollectionInfo startCollection(@RequestParam final String oaiBaseUrl,
		@RequestParam(required = false, defaultValue = "oai_dc") final String oaiFormat,
		@RequestParam(required = false) final String oaiSet,
		@RequestParam(required = false) final LocalDateTime oaiFrom,
		@RequestParam(required = false) final LocalDateTime oaiUntil,
		@RequestParam(required = false, defaultValue = "${oai.conf.maxRecords}") final Long max,
		@RequestParam(required = false) final String notificationEmail) {
		return service.startCollection(oaiBaseUrl, oaiFormat, oaiSet, oaiFrom, oaiUntil, max, notificationEmail);
	}

	@GetMapping("/history/{id}")
	public CollectionInfo getCollectionInfo(@PathVariable final String id) {
		return service.getCollectionInfo(id);
	}

	@GetMapping("/history")
	public Map<String, ExecutionStatus> history(@RequestParam(required = false, defaultValue = "false") final boolean all) {
		return service.history(all);
	}

	@DeleteMapping("/history")
	public Map<String, ExecutionStatus> cleanHistory(@RequestParam(required = false, defaultValue = "false") final boolean all) throws Exception {
		if (all) {
			service.forceExpired();
		}
		service.cronCleanJobs();
		return history(true);
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ErrorMessage handleException(final Exception e) {
		log.error("Error processing http method", e);
		return new ErrorMessage(e);
	}

	public class ErrorMessage {

		private final String message;
		private final String stacktrace;

		public ErrorMessage(final Exception e) {
			this(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}

		public ErrorMessage(final String message, final String stacktrace) {
			this.message = message;
			this.stacktrace = stacktrace;
		}

		public String getMessage() {
			return this.message;
		}

		public String getStacktrace() {
			return this.stacktrace;
		}

	}
}
