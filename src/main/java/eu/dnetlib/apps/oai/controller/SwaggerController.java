package eu.dnetlib.apps.oai.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SwaggerController {

	@GetMapping({
		"/", "apidoc", "api-doc", "/doc", "/swagger"
	})
	public String apiDoc() {
		return "redirect:swagger-ui/index.html";
	}
}
