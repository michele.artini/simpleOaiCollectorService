package eu.dnetlib.apps.oai.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import eu.dnetlib.apps.oai.service.CollectorService;

@Controller
public class ExporterController {

	@Autowired
	private CollectorService service;

	@GetMapping("/download/{id}")
	public ResponseEntity<Resource> download(@PathVariable final String id) throws FileNotFoundException, URISyntaxException {

		final URI uri = new URI(service.getCollectionInfo(id).getStorageUrl());

		if (uri.getScheme().equals("zip") && uri.getPath().endsWith(".zip")) {

			final File file = new File(uri.getPath());

			if (file.exists()) {
				final FileSystemResource resource = new FileSystemResource(file);

				final HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setContentDisposition(ContentDisposition.attachment().filename(file.getName()).build());
				httpHeaders.setLastModified(file.lastModified());
				httpHeaders.setContentLength(file.length());

				return ResponseEntity.ok()
					.headers(httpHeaders)
					.contentLength(file.length())
					.contentType(MediaType.APPLICATION_OCTET_STREAM)
					.body(resource);
			} else {
				throw new FileNotFoundException("File is missing: " + uri);
			}
		} else {
			throw new FileNotFoundException("Data are not exportable, invalid uri: " + uri);
		}
	}

}
